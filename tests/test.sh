#!/bin/bash
# Monolithic test of all of denat functionality
set -e
set -u
set -x
set -o pipefail

TEST_DIR=$(dirname "$(realpath "$0")")
pushd "$TEST_DIR"

# Start the server
denat&
PID=$!
# Kill the server when we exit
cleanup(){
    kill -9 $PID
}
trap cleanup EXIT
popd
counter=0
while [ $counter -lt 100 ]
do
    # poll the server until it answers
    curl --silent --show-error  http://127.0.0.1:8080/version | grep -E "^[[:digit:]]+\.[[:digit:]]+\.[[:digit:]]+" && break
    counter=$((counter+1))
done
if [ $counter -eq 100 ]
then
    exit 1
fi

_token(){
    # Get a new token from the server
    curl --silent --show-error http://127.0.0.1:8080 | grep token0 | awk '{split($4,a,"="); gsub("\"","",a[2]); print a[2]}'
}

upload_magnets(){
    # Upload our magnets
    token=$1
    counter=0
    while read -r line
    do
        counter=$((counter+1))
        src_ip=$(echo "${line}" | awk -F ',' '{print $1}')
        datetime=$(echo "${line}" | awk -F ',' '{print $5}')
        curl --silent --show-error --form token="${token}" --form magnet=osef --form src-ip="${src_ip}" --form datetime="${datetime}" --form interval=1h 127.0.0.1:8080 \
            | grep "You have $counter magnets"
    done < <(tail -n +2 "${TEST_DIR}"/magnets.csv)
}

# NATIVE
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=native --form straws-file=@"${TEST_DIR}"/native/straws.txt 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# France SFR
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the IP<->MSISDN mappings (SFR-specific, see fr-sfr-msisdn.ss)
curl --silent --show-error --form token="${token}" --form format=fr/sfr/msisdn --form straws-file=@"${TEST_DIR}"/fr/sfr-MSISDN.csv 127.0.0.1:8080 | grep "mappings"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'Mappings: 1 errors'
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=fr/sfr/ip --form straws-file=@"${TEST_DIR}"/fr/sfr-IP.csv 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# Spain Vodafone
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=es/vodafone --form straws-file=@"${TEST_DIR}"/es/vodafone.csv 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# Spain Yoigo
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=es/yoigo --form straws-file=@"${TEST_DIR}"/es/yoigo.xlsx 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# France Orange Fortinet
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=fr/orange/fortinet --form straws-file=@"${TEST_DIR}"/fr/orange-fortinet.csv 127.0.0.1:8080 | grep -E "There are [[:digit:]]+ straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# France Orange F5
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=fr/orange/f5 --form straws-file=@"${TEST_DIR}"/fr/orange-f5.csv 127.0.0.1:8080 | grep -E "There are [[:digit:]]+ straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# Spain Orange
## Get the session token from loading the page
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=es/orange --form straws-file=@"${TEST_DIR}"/es/orange.xlsx 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 7) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "7,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"

# Cyprus Epic
## Get a new session token
token=$(_token)
echo "Token is ${token}"
upload_magnets "${token}"
## Upload the haystack
curl --silent --show-error --form token="${token}" --form format=cy/epic --form straws-file=@"${TEST_DIR}"/cy/epic.xlsx 127.0.0.1:8080 | grep "straws"
## Checked that the error we introduced in the file was detected
curl --silent --show-error --form token="${token}" 127.0.0.1:8080 | grep -E 'There were 1 errors'
## We expect James Bond (phone number ending in 007) to match all three magnets
curl --silent --show-error --form token="${token}" 127.0.0.1:8080/results | grep "007,3,"
echo -e "\e[42mTEST PASSED :)\e[0m"
