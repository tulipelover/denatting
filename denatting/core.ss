(import
  :std/iter
  :std/misc/ports
  :std/srfi/1
  :std/srfi/19
  :std/format
  :std/pregexp
  :std/misc/repr
  "utils"
 )
(export #t)

;; CORE DATA STRUCTURES
(defstruct magnet
  ;; Each field is a Maxwell function.
  ;; A Maxwell function is created from an object
  ;; e.g. an IP string such as "127.0.0.1".
  ;; When called with no arguments,
  ;; a Maxwell functions returns the string representation
  ;; of the object that created it, or #f if it
  ;; was created with an empty object or with an #f.
  ;; When called with one argument,
  ;; the Maxwell function returns either
  ;; - #f if the given argument and its underlying object do not match,
  ;; - or #t if they do,
  ;; - or even a number, which can be seen
  ;;   as a distance between the given object and the underlying object,
  ;; - or #t if the argument is #f
  ;;
  ;; Note: All objects match with the empty object.
  ;; i.e. using the Maxwell function of an empty object
  ;; means that you will accept anything because you don't
  ;; have any information that would allow you
  ;; to discriminate.
  ;;
  ;; The fields are Maxwell functions of:
  (src-ip;an IP string e.g. "127.0.0.1"
   src-port;a port string e.g. "3223"
   dest-ip;an IP stirng e.g. "127.0.0.1"
   dest-port;a port stirng e.g. "80"
   datetime);a date (in the srfi 19 sense)
  constructor: :init!)

(defmethod {:pr magnet}
  ;; Pretty print a magnet
  (lambda (self port options)
    (display (format
              "~a~t:~t~a~t->~t~a~t:~t~a~t@~t~a"
              ((magnet-src-ip self))
              ((magnet-src-port self))
              ((magnet-dest-ip self))
              ((magnet-dest-port self))
              ((magnet-datetime self)))
             port)))

(def (maxwell-empty . args)
     ;; The Maxwell function whose underlying object is empty
     (if (null? args)
         ;; when called on no arguments, return #f
         #f
         ;; when called on any argument, return #t, because any object matches
         ;; the empty object
         #t))

(def (maxwell-string s)
     ;;Return the Maxwell function of a string
     ;; that will accept only an exact match with the underlying object.
     ;; The empty object (which will match anything) can be '(), #f or ""
     (if (or (null? s) (not s) (equal? s ""))
         maxwell-empty
         (lambda args
           (if (null? args)
               ;; Return the underlying object
               s
               ;; Compute a match
               (if (not (car args))
                   #t
                   (string=? (car args) s))))))

(def (maxwell-datetime-interval dt i)
     ;; Return the Maxwell function of a date
     ;;  that will accept any date that is within +i or -i
     ;; seconds of dt
     ;; The empty object for dt can be '() or #f
     ;; If the empty object ('(), #f) is given for i, a default interval
     ;; of 1 hour is assumed
     (cond
      ((or (null? dt) (not dt)) maxwell-empty)
      ((or (null? i) (not i)) (maxwell-datetime-interval dt 3600))
      (else (lambda args
              (if (null? args)
                  ;; Return a string representation of the underlying object
                  (format "~a±~as" (date->string dt "~4") i)
                  ;; Compute a match
                  (within-of dt i (car args)))))))

(defmethod {:init! magnet}
  (lambda (self src-ip src-port dest-ip dest-port datetime interval)
    (set! (magnet-src-ip self) (maxwell-string src-ip))
    (set! (magnet-src-port self) (maxwell-string src-port))
    (set! (magnet-dest-ip self) (maxwell-string dest-ip))
    (set! (magnet-dest-port self) (maxwell-string dest-port))
    (set! (magnet-datetime self) (maxwell-datetime-interval datetime interval))))

(defmethod {match? magnet}
  ;; Return the cost of the match between the
  ;; straw and the magnet, and #f if they don't match
  (lambda (self straw)
    (and+
     ((magnet-src-ip    self) (straw-src-ip straw))
     ((magnet-src-port  self) (straw-src-port straw))
     ((magnet-dest-ip   self) (straw-dest-ip straw))
     ((magnet-dest-port self) (straw-dest-port straw))
     ((magnet-datetime  self) (straw-datetime straw)))))

(defstruct straw
  ;; A network event
  (id
   src-ip
   src-port
   dest-ip
   dest-port
   datetime))

(defmethod {:pr straw}
  ;; Pretty print a straw
  (lambda (self port options)
    (display (format "~a~t|~t~a~t:~t~a~t->~t~a~t:~t~a~t@~t~a"
                     (straw-id self)
                     (straw-src-ip self)
                     (straw-src-port self)
                     (straw-dest-ip self)
                     (straw-dest-port self)
                     (date->string (straw-datetime self) "~Y-~m-~dT~H:~M:~S+00:00"))
             port)))

(defstruct accountant
  ;; Holds the magnets and keeps track of the costs
  (;; a map from id to
   ;;  (a map from the magnets to
   ;;    (a (straw cost) tuple)
   costs
   ;; The list of magnets
   magnets)
  constructor: :init!)

(defmethod {:init! accountant}
  (lambda (self magnets)
    (set! (accountant-costs self) (make-hash-table))
    (set! (accountant-magnets self) magnets)))

(defmethod {:pr accountant}
  (lambda (self port options)
    (display (format "Costs are: ~a~nMagnets are: ~a~n"
                     (repr (accountant-costs self))
                     (string-join (map repr (accountant-magnets self)) #\newline))
             port)))

(defmethod {compute-costs! accountant}
  (lambda (self straws)
    (map (lambda (s) {add-cost! self s}) straws)))

(defmethod {add-cost! accountant}
  (lambda (self straw)
    ;; Evaluate and remember the costs of a straw
    ;; For each magnet and its associated cost
    (for  ((m (accountant-magnets self))
           (cost (map (lambda (m)
                        {match? m straw})
                      (accountant-magnets self))))
      ;; If there is a match
      (if cost
        ;; Find the costs already known to the accountant
        (let* ((id (straw-id straw))
               (matching (hash-get
                          (accountant-costs self) id)))
          (if (not matching); No cost known yet
            (hash-put! (accountant-costs self) id
                       (plist->hash-table [m
                                           [straw cost]]))
            ;; Some other magnet already matched for this id
            (hash-put! matching m [straw cost])))))))

;; UTILITY FUNCTIONS
(def (within-of a interval b)
     ;; If a is within interval seconds of b, then
     ;; return the absolute number of seconds between a and b
     ;; else return #f.
     ;;
     ;; a and b are date.
     (let*
         ((pos-int (make-time time-duration 0 interval))
          (neg-int (make-time time-duration 0 (- interval)))
          (atm (date->time-monotonic a))
          (btm (date->time-monotonic b))
          (diff (time-difference atm btm)))
       (if (or (time>? diff pos-int) (time<? diff neg-int))
           #f
           (abs (time-second diff)))))

(def (and+ . args)
  ;; Take a list of args that can be #f, #t or a number
  ;; If any of the args is #f, return #f
  ;; Else, sum the args, with #t meaning 0.
  ;; Warn: in this context, 0 is trueish
  ;; Other languages may make 0 falseish.
  (def (binary-and+ a b)
    (if (not (and a b))
      #f
      (let ((a-val (if (eq? a #t) 0 a))
            (b-val (if (eq? b #t) 0 b)))
        (+ a-val b-val))))
  (fold binary-and+ #t args))
