all:
	./build.ss

test: all tests/fr/orange-f5.csv tests/fr/orange-fortinet.csv tests/es/orange.xlsx tests/cy/epic.xlsx tests/test.sh tests/es/yoigo.xlsx tests/es/vodafone.csv tests/fr/sfr-IP.csv tests/fr/sfr-MSISDN.csv
	tests/test.sh

# Dependency graph of the gerbil code
depgraph.png: denatting/*.ss
	./depgraph.sh
