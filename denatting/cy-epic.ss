(import
  "core"
  "utils"
  "xlsx"
  "tz-str-date"
  :std/pregexp
  :gerbil/gambit/os
  :gerbil/gambit/ports
  :gerbil/gambit/threads
  :hckiang/srfi-54
  :std/format
  :std/iter
  :std/misc/repr
  :std/misc/text
  :std/misc/uuid
  :std/net/httpd
  :std/srfi/19
  :std/misc/list
  :std/srfi/95
  :gerbil/gambit/bytes
  )
(export #t)

;; EPIC
(def (cy-epic-line->straw line)
  ;; Return a straw from a cy epic xlsx line
  (with ([src-ip datetime-str _ _ dst-ip dst-port src-port _ _ _ _ id _ _ _ _]
         line)
    (make-straw
     id
     src-ip
     src-port
     dst-ip
     dst-port
     (datetime-str->datetime datetime-str "~Y-~m-~d ~H:~M:~S" "Europe/Nicosia"))))

(def (cy-epic-file->lines contents)
  (xlsx-open-memory contents (bytes-length contents))
  ;; Discarding the first line (the headers)
  (next-values 16) ;16 elements per line
  (all-remaining-values 16))
