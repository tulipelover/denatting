(import
 "web"
 :std/net/httpd)

(export main)

(def (main . args)
     (set-httpd-max-request-body-length! (expt 2 30));1 GB
     (match args
            ([] (run "0.0.0.0:8080"))
            (["--port" port] (run (string-append "0.0.0.0:" port)))))
